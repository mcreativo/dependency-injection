<?php


abstract class LoggerSingleton implements Logger
{
    /**
     * @var LoggerSingleton $instance
     */
    protected static $instances = [];

    public static function getInstance()
    {

        if (get_called_class() == 'Logger')
            throw new Exception('Cannot instantiate abstract class logger');

        $calledClass = get_called_class();

        if (!isset($instances[$calledClass])) {
            $instances[$calledClass] = new $calledClass();
        }
        return $instances[$calledClass];
    }

    protected function __clone()
    {

    }

    protected function __construct()
    {

    }

} 