<?php


interface Logger
{

    /**
     * @param string $string
     */
    public function log($string);
} 